FROM ubuntu:16.04
RUN apt-get update && apt-get install -y gcc g++ make libpcre++ libpcre++-dev openssl libssl-dev rabbitmq-server
RUN rabbitmq-plugins enable rabbitmq_management
RUN service rabbitmq-server restart
